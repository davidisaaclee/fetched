'use strict';

var _assert = require('assert');

var _assert2 = _interopRequireDefault(_assert);

var _dist = require('../dist');

var _dist2 = _interopRequireDefault(_dist);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe("Fetched", function () {
	it("should construct fetching values", function () {
		_assert2.default.equal(_dist2.default.fetching.isFetching, true);
	});
});