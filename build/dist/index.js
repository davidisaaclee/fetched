'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Fetched = require('./Fetched');

var _Fetched2 = _interopRequireDefault(_Fetched);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _Fetched2.default;