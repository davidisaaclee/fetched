"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// Fetched<T> ::= { isFetching :: Bool, value :: T? }

var Fetched = function () {
	_createClass(Fetched, null, [{
		key: "fetched",
		value: function fetched(value) {
			return new Fetched(false, value);
		}
	}, {
		key: "fetching",
		get: function get() {
			return new Fetched(true, null);
		}
	}]);

	function Fetched(isFetching, value) {
		_classCallCheck(this, Fetched);

		Object.assign(this, { isFetching: isFetching, value: value });
	}

	_createClass(Fetched, [{
		key: "map",
		value: function map(transform) {
			return this.isFetching ? this : new Fetched(false, transform(this.value));
		}
	}, {
		key: "orJust",
		value: function orJust(fallbackValue) {
			return this.isFetching ? fallbackValue : this.value;
		}
	}]);

	return Fetched;
}();

exports.default = Fetched;