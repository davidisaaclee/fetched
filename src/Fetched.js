// Fetched<T> ::= { isFetching :: Bool, value :: T? }

export default class Fetched {
	static fetched(value) {
		return new Fetched(false, value);
	}

	static get fetching() {
		return new Fetched(true, null);
	}

	constructor(isFetching, value) {
		Object.assign(this, { isFetching, value });
	}

	map(transform) {
		return this.isFetching
			? this
			: new Fetched(false, transform(this.value));
	}

	orJust(fallbackValue) {
		return this.isFetching
			? fallbackValue
			: this.value;
	}
}

